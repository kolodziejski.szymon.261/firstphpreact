import "../App.css";
import "./styles/AddPost.css";
import React, {useEffect, useState} from "react";
import {Box, Button} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {useLocalStorage} from "@uidotdev/usehooks";
import env from "react-dotenv";
import Header from "./Header";

function AddPost() {
    const navigate = useNavigate();
    const [token, setToken] = useLocalStorage("token", null);
    const [url, setUrl] = useState();

    useEffect(() => {
        if (!token) {
            navigate("/login");
        }
        getCat();
    }, []);

    function getCat() {
        fetch("https://api.thecatapi.com/v1/images/search")
            .then(response => response.json()).then(data => {
            setUrl(data[0].url);
        });
    }

    const handleAdd = async () => {
        const res = await fetch(`${env.REACT_APP_API_URL}/posts`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token,
            },
            body: JSON.stringify({
                img: url,
            }),
        });
        const json = await res.json();
        console.log(json);
        if (json.message === "Invalid Token") {
            setToken(null);
            navigate("/login");
        }
        if (json.status === "success") {
            navigate("/posts");
            return;
        }
    }

    return (
        <>
            <Header/>
            <Box id={"addForm"}>
                <img src={url} alt="cat"/>
                <div>
                    <Button
                        onClick={handleAdd}
                        variant={"contained"}
                    >Add Post!</Button>
                    <Button
                        onClick={getCat}
                        variant={"outlined"}
                    >Change Cat</Button>
                </div>
            </Box>
        </>
    );
}

export default AddPost;