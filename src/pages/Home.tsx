import React, {useState} from "react";
import "../App.css";
import "./styles/Home.css";
import Header from "./Header";

function Home() {
    const [url, setUrl] = useState("");
    React.useEffect(() => {
        fetch("https://api.thecatapi.com/v1/images/search")
            .then(response => response.json()).then(data => {
            setUrl(data[0].url);
        });
    }, []);
    return (
        <>
            <Header/>
            <div id={"home"}>
                <section>
                    <div>
                        <h2>About</h2>
                        <p>
                            First PHP and REACT project in a while.
                            <br/>Implements:
                        </p>
                        <ul>
                            <li>React frontend</li>
                            <li>MaterialUI</li>
                            <li>self made PHP REST API</li>
                            <li>MariaDB database</li>
                            <li>Utilizing <a href="https://thecatapi.com/">CAT API</a></li>
                        </ul>
                    </div>
                    <div>
                    <h2>Features</h2>
                    <ul>
                        <li>Registration and Login system</li>
                        <li>JWT tokens</li>
                        <li>Random cats</li>
                        <li>Saving cats by posting them</li>
                    </ul>
                    </div>
                    <div>
                    <h2>Made by</h2>
                    <ul>
                        <li>Szymon Kołodziejski</li>
                        <li><a href="https://gitlab.com/kolodziejski.szymon.261">GitLab</a></li>
                    </ul>
                    </div>
                </section>
                <img src={url} alt=""/>
            </div>
        </>
    );
}

export default Home;