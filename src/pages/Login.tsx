import "../App.css";
import "./styles/LogReg.css";
import React, {useEffect, useState} from "react";
import {Box, Button, TextField} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {useLocalStorage} from "@uidotdev/usehooks";
import env from "react-dotenv";

function Login() {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [token, setToken] = useLocalStorage("token", null);
    useEffect(() => {
        if (token) {
            navigate("/posts");
        }
    });

    const handleLogin = async () => {
        const res = await fetch(`${env.REACT_APP_API_URL}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name,
                password,
            }),
        });
        const json = await res.json();
        if (json.token) {
            setToken(json.token);
            navigate("/posts");
            return;
        }
        alert(json.message || "Error logging in!");
    }

    return (
        <Box id={"form"}>
            <img onClick={()=>navigate("/")} src="https://cdn2.thecatapi.com/images/3v0.jpg" alt="cat"/>
            <TextField
                required
                id={"name"}
                label={"Name"}
                onChange={(e) => setName(e.target.value)}
                value={name}
                autoFocus
            />
            <TextField
                required
                id={"password"}
                label={"Password"}
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                type={"password"}
            />
            <Button
                onClick={handleLogin}
                variant={"contained"}
            >Log in</Button>
            <div id={"link"}>
                <span onClick={()=>navigate("/register")}>Register</span>
            </div>
        </Box>
    );
}

export default Login;