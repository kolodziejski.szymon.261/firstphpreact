import React, {useEffect, useState} from "react";
import {Button} from "@mui/material";
import {useNavigate} from "react-router-dom";
import "../App.css";
import {useLocalStorage} from "@uidotdev/usehooks";


function Header() {
    const navigate = useNavigate();
    const [token, setToken] = useLocalStorage("token", null);
    const [reload, setReload] = useState(1)
    const logout = () => {
        setToken(null);
        navigate("/");
    }
    useEffect(() => {
        function handleResize() {
            if (window.innerWidth < 620) {
                setReload(reload + 1)
            }
            if (window.innerWidth > 620) {
                setReload(reload + 1)
            }
        }

        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    });

    return (
        <header key={reload}>
            {window.innerWidth > 620 ?
                <h1 style={{cursor: "pointer"}} onClick={() => navigate("/")}>Awesome cat page!</h1>
                :
                <h1 style={{cursor: "pointer"}} onClick={() => navigate("/")}>ACP</h1>
            }

            <div className="divider"></div>
            {!token ? <>
                <Button variant={"text"} onClick={() => navigate("/login")}>Login</Button>
                <Button variant={"contained"} onClick={() => navigate("/register")}>Register</Button>
            </> : <>
                {window.innerWidth > 770 &&
                    <Button variant={"text"} onClick={() => navigate("/addPost")}>Add Post</Button>}
                <Button variant={"text"} onClick={() => navigate("/posts")}>Posts</Button>
                <Button variant={"contained"} onClick={logout}>logout</Button>
            </>}
        </header>
    )
}

export default Header;