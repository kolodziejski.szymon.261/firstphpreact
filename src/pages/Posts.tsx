import React, {useEffect, useState} from "react";
import "../App.css";
import "./styles/Posts.css";
import {Fab, IconButton, ImageList, ImageListItem, ImageListItemBar} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {useLocalStorage} from "@uidotdev/usehooks";
import env from "react-dotenv";
import Header from "./Header";
import AddIcon from '@mui/icons-material/Add';

class User {
    id: number;
    name: string;
    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}
class Post {
    id: number;
    userId: number;
    img: string;
    constructor(id: number, userId: number, img: string) {
        this.id = id;
        this.userId = userId;
        this.img = img;
    }
}

function Posts() {
    const navigate = useNavigate();
    const [token, setToken] = useLocalStorage("token", null);
    const [users, setUsers] = useState<User[]>([]);
    const [posts, setPosts] = useState<Post[]>([]);
    const [columns, setColumns] = useState(3)
    useEffect(() => {
        if (!token) {
            navigate("/login");
        }

        async function fetchCats() {
            const res = await fetch(`${env.REACT_APP_API_URL}/posts`, {
                method: "GET",
                headers: {
                    'Authorization': 'Bearer ' + token,
                }
            });
            const json = await res.json();
            if (json.status !== 'success') {
                console.error(json.message);
                if (json.message === "Invalid Token") {
                    setToken(null);
                    navigate("/login");
                }
                return;
            }
            setPosts(json.posts);

            return;
        }

        fetchCats();

        const handleResize = () => {
            if (window.innerWidth < 770) {
                setColumns(1);
            } else if (window.innerWidth < 1280) {
                setColumns(2);
            } else {
                setColumns(3);
            }
        }
        handleResize();
        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };

    }, []);

    useEffect(() => {
        async function fetchUsers() {
            const aUsers: User[] = [];
            for (let i = 0; i < posts.length; i++) {
                if (aUsers.find(user => user.id === posts[i].userId))
                    continue;
                const res = await fetch(`${env.REACT_APP_API_URL}/users?id=${posts[i].userId}`, {
                    method: "GET",
                    headers: {
                        'Authorization': 'Bearer ' + token,
                    }
                });
                const json = await res.json();
                if (json.status != 'success') {
                    console.error(json.message);
                    return;
                }
                aUsers.push(json.user);
            }
            if (aUsers.length > 0)
                setUsers(aUsers);
        }
        fetchUsers();
    }, [posts]);


    return (
        <>
            <Header/>
            <div id={"posts"}>
                {window.innerWidth < 770 &&
                    <Fab color="primary" aria-label="add" id={"addFab"} onClick={() => navigate("/addPost")}>
                        <AddIcon/>
                    </Fab>}
                {posts.length > 0 && (
                    <ImageList variant={"masonry"} cols={columns} gap={8}>
                        {posts.map((post) => (
                            <ImageListItem key={post.img}>
                                <img
                                    src={post.img}
                                    alt={post.img}
                                    loading="lazy"
                                />
                                <ImageListItemBar
                                    title={"Post id: " + post.id}
                                    subtitle={"User: " + users.find(user => user.id === post.userId)?.name}
                                    actionIcon={
                                        <IconButton
                                            sx={{color: 'rgba(255, 255, 255, 0.54)'}}
                                            aria-label={`info about ${post.userId}`}
                                        >
                                        </IconButton>
                                    }
                                />
                            </ImageListItem>
                        ))}
                    </ImageList>
                )}
            </div>
        </>
    );
}

export default Posts;