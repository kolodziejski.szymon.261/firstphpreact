import "../App.css";
import "./styles/LogReg.css";
import React, {useEffect} from "react";
import {Box, Button, TextField} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {useLocalStorage} from "@uidotdev/usehooks";
import env from "react-dotenv";

function Register() {
    const navigate = useNavigate();
    const [token, setToken] = useLocalStorage("token", null);
    useEffect(() => {
        if (token) {
            navigate("/posts");
        }
    });

    const handleRegister = async () => {
        const name = (document.getElementById("name") as HTMLInputElement).value;
        const password = (document.getElementById("password") as HTMLInputElement).value;
        const res = await fetch(`${env.REACT_APP_API_URL}/users`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name,
                password,
            }),
        });
        const json = await res.json();
        if(json.status === "success") return navigate("/login");
        alert(json.message || "Error registering!");
    }

    return (
        <Box id={"form"}>
            <img onClick={() => navigate("/")} src="https://cdn2.thecatapi.com/images/3v0.jpg" alt="cat"/>
            <TextField
                required
                id={"name"}
                label={"Name"}
                autoFocus
            />
            <TextField
                required
                id={"password"}
                label={"Password"}
                type={"password"}
            />
            <Button
                onClick={handleRegister}
                variant={"contained"}
            >Register</Button>
            <div id={"link"}>
                <span onClick={() => navigate("/login")}>Log in</span>
            </div>
        </Box>
    );
}

export default Register;